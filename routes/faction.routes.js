const express = require('express');
const router = express.Router();

const Faction = require('../models/faction');

router.get('/', async (req, res, next) => {
	try {
		const factions = await Faction.find().populate('characters');
		return res.status(200).json(factions)
	} catch (error) {
		return next(error)
	}
});

router.post('/', async (req, res, next) => {
    try{
        const newFaction = new Faction({
            name: req.body.name,
            location: req.body.location,
            characters: []
        });

        const createdFaction = await newFaction.save();
        return res.status(201).json(createdFaction);
    }catch(error){
        return next(error)
    }
});

router.delete('/:id', async (req, res, next) => {
    try{
        const {id} = req.params;
        const factionDeleted = await Faction.findByIdAndDelete(id);
        return res.status(200).json(factionDeleted);
    }catch(error){
        return next(error)
    }
});

router.put('/add-faction', async (req, res, next) => {
    try {
        const { factionId } = req.body;
        const { characterId } = req.body;
        const updatedFaction = await Faction.findByIdAndUpdate(
            factionId,
            { $push: { characters: characterId } },
            { new: true }
        );
        return res.status(200).json(updatedFaction);
    } catch (error) {
        return next(error);
    }
});

module.exports = router;