const express = require('express');
const router = express.Router();
const Character = require('../models/character');

router.get('/', async (req, res, next) => {
	try {
		const characters = await Character.find();
		return res.status(200).json(characters)
	} catch (err) {
		return next(error);
	}
});

router.get('/id/:id', async (req, res, next) => {
	const id = req.params.id;
	try {
		const character = await Character.findById(id);
		if (character) {
			return res.status(200).json(character);
		} else {
			return res.status(404).json('No character found by this id');
		}
	} catch (err) {
		return next(error);
	}
});

router.get('/name/:name', async (req, res, next) => {
	const {name} = req.params;

	try {
		const charactersByName = await Character.find({ name });
		return res.status(200).json(charactersByName);
	} catch (err) {
		return next(error);
	}
});

router.get('/faction/:faction', async (req, res, next) => {
	const {faction} = req.params;

	try {
		const charactersByFaction= await Character.find({ faction });
		return res.status(200).json(charactersByFaction);
	} catch (err) {
		return next(error);
	}
});

router.get('/age/:age', async (req, res, next) => {
	const {age} = req.params;

	try {
		const charactersByAge = await Character.find({ age: {$gt:age} });
		return res.status(200).json(charactersByAge);
	} catch (err) {
		return next(error);
	}
});

router.post('/create', async (req, res, next) => {
    try {
        const newCharacter = new Character({
            name: req.body.name,
            especialization: req.body.especialization,
            faction: req.body.faction,
            age: req.body.age
        });

        const createdCharacter = await newCharacter.save();
        return res.status(201).json(createdCharacter);
    } catch (error) {
        return next(error);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        const {id} = req.params;
        const characterDeleted = await Character.findByIdAndDelete(id);
        return res.status(200).json(characterDeleted);
    } catch (error) {
        return next(error);
    }
});

router.put('/edit/:id', async (req, res, next) => {
    try {
        const { id } = req.params 
        const characterModify = new Character(req.body) 
        characterModify._id = id 
        const characterUpdated = await Character.findByIdAndUpdate(id , characterModify)
        return res.status(200).json(characterUpdated)//Este personaje que devolvemos es el anterior a su modificación
    } catch (error) {
        return next(error)
    }
});


module.exports = router;