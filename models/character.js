const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const characterSchema = new Schema(
  {
    name: { type: String, required: true },
    especialization: { type: String, required: true },
    faction: { type: Number },
    age: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);

const character = mongoose.model('Character', characterSchema);
module.exports = character;