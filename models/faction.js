const mongoose = require('mongoose');

const factionSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    location: { type: String, required: true },
    members: [{ type: mongoose.Types.ObjectId, ref: 'Character' }],
  },
  {
    timestamps: true,
  }
);

const faction = mongoose.model('Faction', factionSchema);
module.exports = faction;
