const mongoose = require('mongoose');

const Movie = require('../models/character');

const characters = [
  {
    name: 'Thrall',
    especialization: 'Chaman',
    faction: 'Horde',
    age: 30,
  },
  {
    name: 'Jaina',
    especialization: 'Mage',
    faction: 'Alliance',
    age: 36,
  },
  {
    name: 'LorThemar Theron',
    especialization: 'Hunter',
    faction: 'Horde',
    age: 38,
  },
  {
    name: 'Anduin',
    especialization: 'Paladin',
    faction: 'Alliance',
    age: 20,
  },
  {
    name: 'Baine Bloodhoof',
    especialization: 'Druid',
    faction: 'Horde',
    age: 43,
  },
  {
    name: 'Genn Greymane',
    especialization: 'Warrior',
    faction: 'Alliance',
    age: 77,
  },
];
const characterDocuments = characters.map(character => new Character(character));
mongoose
  .connect('mongodb+srv://albert-beltran:<Alboraya1>@cluster0.oriqdnn.mongodb.net/?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
    const allCharacters = await Character.find();
    if (allCharacters.length) {
      await Character.collection.drop(); 
    }
  })
  .catch((err) => console.log(`Error deleting data: ${err}`))
  .then(async () => {
		await Character.insertMany(characterDocuments);
    console.log('DatabaseCreated')
	})
  .catch((err) => console.log(`Error creating data: ${err}`))
  .finally(() => mongoose.disconnect());