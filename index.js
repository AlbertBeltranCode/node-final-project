const express = require('express');
const {connect} = require('./utils/db')

const charactersRouter = require('./routes/character.routes')
const factionsRouter = require('./routes/faction.routes')

connect();

const PORT = 3000;
const server = express();

server.use(express.json());
server.use(express.urlencoded({ extended: false }));

server.use('/characters', charactersRouter);
server.use('/factions', factionsRouter)

server.use((error, req, res, next) => {
	return res.status(error.status || 500).json(error.message || 'Unexpected error');
});

server.listen(PORT, () => {
  console.log(`Server running in port ${PORT}`);
});